readyToReset

a flag variable

A flag variable, in its simplest form, is a variable you define to have one value until some condition is true, in which case you change the variable's value. Using flag variable(user defined variable, not a predefined) you can control the flow of a function or statement, allowing you to check for certain conditions while your function progresses.