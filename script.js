class Calculator {

    constructor(previousOperandTextElement, currentOperandTextElement) {

        this.previousOperandTextElement = previousOperandTextElement
        this.currentOperandTextElement = currentOperandTextElement
        //flag variable
        this.readyToReset = false;

        this.clear()
    }

    clear() {
        this.currentOperand = ''
        this.previousOperand = ''
        this.operation = undefined
    }

    delete() {
        // on convertie en string
        // 
        // 0 est le premier
        // -1 est toujours le dernier
        //The slice() method selects the elements starting at the given start 
        //argument, and ends at, but does not include, the given end argument
        this.currentOperand = this.currentOperand.toString().slice(0, -1)
    }

    appendNumber(number) {
        // pour empecher d'adicionner pluisers "."
        if (number === "." && this.currentOperand.includes('.')) return
        // on converti l'affichage en string pour empecher q au lien de 
        // ajouter a la fin, ça adicione 
        this.currentOperand = this.currentOperand.toString() + number.toString()
    }

    chooseOperation(operation) {
        // on empeche la function de se declencher s'on a pas de numeros
        if (this.currentOperand === '') return
        // si on avait un numero en haut
        // ça fait le calcul et display le resultat
        if (this.previousOperand !== '') {
            this.compute()
        }
        this.operation = operation
        // on envoie le numero en haut
        this.previousOperand = this.currentOperand
        // on clear le dislay de em bas
        this.currentOperand = ''
    }

    compute() {
        // to stock the result of this function
        let computation
        // on passe de string à integer
        const prev = parseFloat(this.previousOperand)
        const current = parseFloat(this.currentOperand)
        // on empeche la function de se declancher s'il y a pas les 2 parties
        if (isNaN(prev) || isNaN(current)) return
        switch (this.operation) {
            case '+':
                computation = prev + current
                break
            case '-':
                computation = prev - current
                break
            case '*':
                computation = prev * current
                break
            case '÷':
                computation = prev / current
                break
            default:
                return
        }
        this.readyToReset = true;
        // on atribue le resultat
        this.currentOperand = computation
        // et on "clear" l'operacion et son affichage
        this.operation = undefined
        this.previousOperand = '';
    }
    
    updateDisplay() {
        // on atribue le numero currant à l'html pour l'aficher
        this.currentOperandTextElement.innerText = this.currentOperand
        // on regarde s'il y a une operation en cours
        if (this.operation != null) {
            // et si c'est le cas, on la concatene et on afiche le tout
            // dans la partie haut
            this.previousOperandTextElement.innerText = `${this.previousOperand} ${this.operation}`
        } else {
            // si c'est pas le cas, on affiche rien en haut
            // (ça sert a clear quand on fait une operation)
            this.previousOperandTextElement.innerText = ''
        }
    }
}


const numberButtons = document.querySelectorAll('[data-number]');
const operationButtons = document.querySelectorAll('[data-operation]');
const equalsButton = document.querySelector('[data-equals]');
const deleteButton = document.querySelector('[data-delete]');
const allClearButton = document.querySelector('[data-all-clear]');
const previousOperandTextElement = document.querySelector('[data-previous-operand]');
const currentOperandTextElement = document.querySelector('[data-current-operand]');

const calculator = new Calculator(previousOperandTextElement, currentOperandTextElement)


numberButtons.forEach(button => {
    // auqnd on clique un button
    button.addEventListener("click", () => {
        // si on avait un resultat anterior, on le clear 
        if (calculator.previousOperand === "" &&
            calculator.currentOperand !== "" &&
            calculator.readyToReset) {
            calculator.currentOperand = "";
            calculator.readyToReset = false;
        }
        // on "append" le contenue du button (le chiffre)
        // en appelant la function qu'on a fait
        calculator.appendNumber(button.innerText)
        // et on afiche le numero en display actualisé
        calculator.updateDisplay();
    })
})

operationButtons.forEach(button => {
    // auqnd on clique un button
    button.addEventListener('click', () => {
        // on apelle la function
        calculator.chooseOperation(button.innerText)
        // et on afiche le display actualisé
        calculator.updateDisplay()
    })
})

//  function pour le button "="
equalsButton.addEventListener('click', button => {
    // fait l'operation
    calculator.compute()
    // montre le resultat
    calculator.updateDisplay()
})

//  function pour le button "all clear"
allClearButton.addEventListener('click', button => {
    calculator.clear()
    calculator.updateDisplay()
})

//  function pour le button "delete"
deleteButton.addEventListener('click', button => {
    calculator.delete()
    calculator.updateDisplay()
})